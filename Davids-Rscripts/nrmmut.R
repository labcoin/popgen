
# nrmmut - simulate genotype proportions under drift

ngen = 2000
popsize = 500
p = 0.8
mu = 0.01
gc = matrix(0,ngen,3)	      # initialize matrix for genotype counts
for(i in 1:ngen){		# start evolution

# sample genotypes in the next generation

  pp = p*(1-mu)+(1-p)*mu
  geno = sample(2:0,popsize,repl=T,prob=c(pp^2,2*pp*(1-pp),(1-pp)^2))

# obtain the counts for the three genotypes

  gc[i,] = hist(geno,br=seq(-0.5,2.5,1),plot=F)$c

  p = mean(geno)/2	# update the value of p
}

apply(gc,2,mean)

matplot(gc,type="l",xlim=c(0,ngen*1.3),ylim=c(0,popsize),lty=1,lwd=2,xlab="generation",ylab="genotype count",main=paste("Genotype counts under drift, popsize=",popsize,", mutation rate",mu))
legend(ngen,popsize*1.04,leg=c("AA","Aa","aa"),lty=1,lwd=2,col=1:3,cex=1.8)

plot(gc[,2]/popsize,type="l",lwd=2,ylim=c(0,1),xlab="generation",ylab="heterozygosity",main=paste("Heterozygosity under drift, popsize=",popsize,", mutation rate",mu))
p = (gc[,1]+gc[,2]/2)/popsize
lines(2*p*(1-p),col=2,lwd=2)
legend(0,1,leg=c("observed","HWE expected"),lty=1,lwd=2,col=1:2,cex=1.8)
