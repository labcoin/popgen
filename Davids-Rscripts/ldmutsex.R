# ldmut.R
rep=10

##pp is frequency of (AB,Ab,aB, ab)
mutate<-function(pp,mu){
  p = rep(0,4)
  p[1] = sum(pp*c((1-mu)^2,mu*(1-mu),mu*(1-mu),mu^2))
  p[2] = sum(pp*c(mu*(1-mu),(1-mu)^2,mu^2,mu*(1-mu)))
  p[3] = sum(pp*c(mu*(1-mu),mu^2,(1-mu)^2,mu*(1-mu)))
  p[4] = sum(pp*c(mu^2,mu*(1-mu),mu*(1-mu),(1-mu)^2))
  p
}
##pp is frequency of (AB,Ab,aB, ab) before recomb
recomFreqs<-function(pp,th){
 p1 = pp[1]+pp[2]; p2 = pp[1]+pp[3]

 freqnew = pp*(1-th)+th*c(p1*p2,p1*(1-p2),(1-p1)*p2,(1-p1)*(1-p2))
 freqnew
}


calcLD<-function(pp){
  ldstat = rep(0,2)
  p1 = pp[1]+pp[2]; p2 = pp[1]+pp[3]
n = sum(pp)
 # D00 = pp[1]-p1*p2
 # if(D00>0) ldstat[1] = D00/min(p1*(1-p2),p2*(1-p1)) else ldstat[1] = -D00/min(p1*p2,(1-p2)*(1-p1))
  ldstat[1] = (1 - p1^2 - (1-p1)^2)*((2*n)/(2*n-1))
  ldstat[2] = (pp[1]*pp[4]-pp[2]*pp[3])^2/(p1*p2*(1-p1)*(1-p2))
  ldstat
}

##popsizeFM number of maternally inherited X alleles in females
##popsizeM number of X alleles in male (effective sze)
##th recombination rate between loci
##mu=mutation rate at loci
##init used to get initial distribution of freqs of (AB,Ab,aB,ab)
simulate<-function(ngen=100, popsizeFM=1000,popsizeM=10,  th=0.001, mu=0.0005,init=c(6,2,0,2),plot=FALSE){
 popsizeFP = popsizeFM;
 popsizeT=popsizeFP+popsizeFM+popsizeM*2

 hpm = matrix(0,ngen,4); hpm[1,] = init/sum(init); #xmale hap freqs (reproductive males)
 hpmall = matrix(0,ngen,4); hpmall[1,] = init/sum(init); #xmale hap freqs (all males)
 hpf = matrix(0,ngen,4); hpf[1,] = init/sum(init); #xfemale hap freqs
 hpa=matrix(0,ngen,4);hpa[1,] = init/sum(init);  ##autosomes hap freqs (AB,Ab,aB,ab)

 hp=matrix(0,ngen,4); #xaverage male and female

 ldstat = matrix(0,ngen,2) # x chrom ld stats
 ldstata = matrix(0,ngen,2) # autosome ld stats

for(i in 2:ngen){
# haplotype proportions in next generation after recombination 
#  calculate sampling freqs after recombination and mutation
  pm = mutate(recomFreqs(hpf[i-1,],th),mu) #maternal x contribution
  pp = mutate(hpm[i-1,],mu) #paternal X contribution. no recombs
  pa = mutate(recomFreqs(hpa[i-1,],th),mu) #autosomal


# sample haplotypes in next generation and record counts
  tmpm = sample(1:4,popsizeM,repl=T,prob=pm)  ##for male children which go on to reproduce
  tmpmall = sample(1:4,popsizeFM,repl=T,prob=pm)  ##for all male children
  tmpmf1 = sample(1:4,popsizeFM,repl=T,prob=pm)  ##for maternal contrib to female
  tmpmf2 = sample(1:4,popsizeFP,repl=T,prob=pp)  ##for paternal contrib to female
  tmpa = sample(1:4,popsizeT,repl=T,prob=pa)  ##autosomes
  hpm[i,] = hist(tmpm,br=seq(0.5,4.5,1),plot=F)$c/popsizeM
  hpmall[i,] = hist(tmpmall,br=seq(0.5,4.5,1),plot=F)$c/popsizeFM
  hpf[i,] = hist(c(tmpmf1,tmpmf2),br=seq(0.5,4.5,1),plot=F)$c/(popsizeFM+popsizeFP)
  hpa[i,] = hist(tmpa,br=seq(0.5,4.5,1),plot=F)$c/popsizeT
  ldstat[i,] = calcLD((2*hpf[i,]+hpmall[i,])/3) 
  ldstata[i,] = calcLD(hpa[i,]) 
}
hp = hp[-1,]; ldstat = ldstat[-1,]
hpa = hpa[-1,]; ldstata = ldstata[-1,]
if(plot){
par(mfrow=c(2,1),mar=c(3,2,2,1))
#matplot(hp,type="l",xlim=c(0,ngen*1.3),ylim=c(0,1),lty=1,lwd=2,xlab="",ylab="haplotype proportion",main=paste("LD: drift + mut; pop size=", popsize," theta=",th,", mu=",mu))
#legend(ngen,1,leg=c("AB","Ab","aB","ab"),lty=1,col=1:4,lwd=2,cex=1.8)
cols = c(5,6)
lty = c(1)
matplot(cbind(ldstat[,1,drop=F],ldstata[,1,drop=F]),type="l",xlim=c(0,ngen*1.3),ylim=c(0,1),lty=lty,lwd=2,col=cols,xlab="",ylab="")
legend(ngen,1,leg=c("|het|_X","|het|_aut*0.75"),lty=lty,col=cols,lwd=2,cex=1)
matplot(cbind(ldstat[,2,drop=F],ldstata[,2,drop=F]*0.75),type="l",xlim=c(0,ngen*1.3),ylim=c(0,1),lty=lty,lwd=2,col=cols,xlab="",ylab="")
legend(ngen,1,leg=c("r^2_X","r^2_aut"),lty=lty,col=cols,lwd=2,cex=1)
}
#print(ldstat[ngen-1,],)
c(ldstat[ngen-1,],ldstata[ngen-1,])
}

sim<-function(reps){

resld =matrix(0,nrow=reps,ncol=4) 
resld1 =matrix(0,nrow=reps,ncol=4) 
resld2 =matrix(0,nrow=reps,ncol=4) 
resld3 =matrix(0,nrow=reps,ncol=4) 

for(i in 1:reps){
	resld[i,] = simulate(ngen=500,popsizeFM=527,popsizeM=527,th=1e-5,mu=1e-7,init=c(6,2,0,2),plot=FALSE)
	resld1[i,] = simulate(ngen=500,popsizeFM=700,popsizeM=353,th=1e-5,mu=1e-7,init=c(6,2,0,2),plot=FALSE)
	resld2[i,] = simulate(ngen=500,popsizeFM=3400,popsizeM=2852,th=1e-5,mu=1e-7,init=c(6,2,0,2),plot=FALSE)

	resld3[i,] = simulate(ngen=500,popsizeFM=3126,popsizeM=3126,th=1e-5,mu=1e-7,init=c(6,2,0,2),plot=FALSE)
 #print(apply(resld3[1:i,,drop=F],2,mean,na.rm=T))
# print(apply(resld4[1:i,,drop=F],2,mean,na.rm=T))
}
resldalln = list(resld,resld1,resld2,resld3)
names(resldalln) = list("527F_527M", "700F_353M", "3400F_2852M","3126F_3126M") 
resldalln
}

getCI<-function(v,n){
sqrt(v)/sqrt(n)*1.96
}

error.bar <- function(x, y, upper, lower=upper, length=0.1,...){
    if(length(x) != length(y) | length(y) !=length(lower) | length(lower) != length(upper))
    stop("vectors must be same length")
    arrows(x,y+upper, x, y-lower, angle=90, code=3, length=length, ...)
 }


gR<-function(resldall){
avg = matrix(0,nrow=length(resldall), ncol=2)
var = matrix(0,nrow=length(resldall), ncol=2)
for(k in 1:length(resldall)){
i=3000
var[k,] =  apply(resldall[[k]][1:i,,drop=F],2,var,na.rm=T)[c(2,4)]
avg[k,] = apply(resldall[[k]][1:i,,drop=F],2,mean,na.rm=T)[c(2,4)]
}
dimnames(avg)=list(names(resldall),c("chrX","chr1-chr22" ))
dimnames(var)=list(names(resldall),c("chrX","chr1-chr22" ))
pdf("SuppFig1.pdf")
CI = apply(var,c(1,2),getCI,3000)

 b = barplot(t(avg),beside=T,legend.text=dimnames(avg)[[2]],col=c("LIGHTGRAY","WHITE"),ylim=c(0,0.8)
	,xlab="Simulation", ylab="Linkage Disequilibrium (r^2)",cex.names=0.5)


error.bar(b[1,],avg[,1],CI[,1])
error.bar(b[2,],avg[,2],CI[,2])
dev.off()
#,names.arg = names(resldall))
}


resldnall = sim(rep)
save(resldnall,"resfile")

