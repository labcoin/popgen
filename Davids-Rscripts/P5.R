dat5 = matrix(scan("Data5.txt"),52,200)
tmp = table(dat5[1,],dat5[2,])
plot(0:2,tmp[1,]/(tmp[1,]+tmp[2,]),col=4)
fisher.test(tmp)
prop.trend.test(tmp[1,],tmp[1,]+tmp[2,])
stat = rep(0,50)
for(i in 3:52){
tmp = table(dat5[1,],dat5[i,])
case = tmp[1,]
cont = tmp[2,]
stat[i-2] = prop.trend.test(case,case+cont)$stat
}
stat
median(stat)
lam = max(1,median(stat)/qchisq(0.5,1))
lam

dat6 = read.table("Data6.txt",head=T)
lm0 = lm(y~sex,dat=dat6)
summary(lm0)
lm1 = lm(y~geno,dat=dat6)
summary(lm1)
plot(y~geno,dat=dat6)
lines(fitted(lm1)~geno,col=7,lwd=2,dat=dat6)
lm2 = lm(y~sex+geno,dat=dat6)
summary(lm2)














